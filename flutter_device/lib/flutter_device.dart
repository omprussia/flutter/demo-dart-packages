// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:device_platform_interface/device_platform_interface.dart';

class FlutterDevice {
  /// Фабричный конструктор
  factory FlutterDevice() {
    _singleton ??= FlutterDevice._();
    return _singleton!;
  }

  /// Приватный конструктор
  FlutterDevice._();

  /// Инстанс класса
  static FlutterDevice? _singleton;

  /// Инстанс DevicePlatform
  static DevicePlatform get _platform {
    return DevicePlatform.instance;
  }

  /// Реализация метода
  Future<String?> get deviceName {
    return _platform.deviceName;
  }
}
