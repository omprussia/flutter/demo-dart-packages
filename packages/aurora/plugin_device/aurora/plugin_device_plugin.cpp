/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <plugin_device/plugin_device_plugin.h>
#include <flutter/method-channel.h>
#include <QtDBus/QtDBus>

/**
 * Регистарция [MethodChannel].
 */
void PluginDevicePlugin::RegisterWithRegistrar(PluginRegistrar &registrar)
{
    registrar.RegisterMethodChannel("plugin_device",
                                    MethodCodecType::Standard,
                                    [this](const MethodCall &call) { this->onMethodCall(call); });
}

/**
 * Метод onMethodCall будет выполняться при вызове MethodChannel из Dart плагина.
 * По названию передаваемому из плагина можно вызвать нужный платфором-зависимый метод.
 */
void PluginDevicePlugin::onMethodCall(const MethodCall &call)
{
    const auto &method = call.GetMethod();
    if (method == "getDeviceName") {
        onGetDeviceName(call);
        return;
    }
    unimplemented(call);
}

/**
 * Платформо-зависимый метод получающий название устройства
 */
void PluginDevicePlugin::onGetDeviceName(const MethodCall &call)
{
    if (!QDBusConnection::sessionBus().isConnected()) {
        call.SendSuccessResponse(nullptr);
        return;
    }
    QDBusInterface iface("ru.omp.deviceinfo",
        "/ru/omp/deviceinfo/Features",
        "",
        QDBusConnection::sessionBus()
    );
    if (iface.isValid()) {
        QDBusReply<QString> reply = iface.call("getDeviceModel");
        if (reply.isValid()) {
            call.SendSuccessResponse(reply.value().toStdString());
            return;
        }
    }
    call.SendSuccessResponse(nullptr);
}

/**
 * Метод возвращающий [nullptr] если запрашиваемый метод не найден
 */
void PluginDevicePlugin::unimplemented(const MethodCall &call)
{
    call.SendSuccessResponse(nullptr);
}
