/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
#ifndef FLUTTER_PLUGIN_PLUGIN_DEVICE_PLUGIN_H
#define FLUTTER_PLUGIN_PLUGIN_DEVICE_PLUGIN_H

#include <flutter/plugin-interface.h>
#include <plugin_device/globals.h>

class PLUGIN_EXPORT PluginDevicePlugin final : public PluginInterface
{
public:
    void RegisterWithRegistrar(PluginRegistrar &registrar) override;

private:
    void onMethodCall(const MethodCall &call);
    void onGetDeviceName(const MethodCall &call);
    void unimplemented(const MethodCall &call);
};

#endif /* FLUTTER_PLUGIN_PLUGIN_DEVICE_PLUGIN_H */
