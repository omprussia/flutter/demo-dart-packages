// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:device_platform_interface/device_platform_interface.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

/// Реализация [PluginDevicePlatform], которая использует каналы методов.
class MethodChannelPluginDevice extends DevicePlatform {
  /// Канал метода, используемый для взаимодействия с собственной платформой.
  @visibleForTesting
  final methodChannel = const MethodChannel('plugin_device');

  /// Реализация метода получения названия устройства
  /// getDeviceName - название метода ктороый можно проверить
  /// в платфоромо-зависимой части плагина
  @override
  Future<String?> get deviceName async {
    return await methodChannel.invokeMethod<String>('getDeviceName');
  }
}
