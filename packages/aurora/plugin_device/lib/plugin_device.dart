// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:device_platform_interface/device_platform_interface.dart';
import 'package:plugin_device/plugin_device_method_channel.dart';

class PluginDevice extends DevicePlatform {
  /// Метод который выполнится при старте приложения
  /// В этом методе можно установить платформо-зависимый плагин
  static void registerWith() {
    DevicePlatform.instance = MethodChannelPluginDevice();
  }

  /// Реализация метода инетфрайса [DevicePlatform]
  @override
  Future<String?> get deviceName => DevicePlatform.instance.deviceName;
}
