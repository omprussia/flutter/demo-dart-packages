// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'dart:async';

import 'package:ffi_plugin_device/ffi_plugin_device.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _deviceName = 'Unknown';
  final _dartPackage = FFIPluginDevice();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String deviceName;
    try {
      deviceName = await _dartPackage.deviceName ?? _deviceName;
    } catch (e) {
      deviceName = e.toString();
    }

    if (!mounted) return;

    setState(() {
      _deviceName = deviceName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('FFI plugin example'),
        ),
        body: Center(
          child: Text('Device name: $_deviceName\n'),
        ),
      ),
    );
  }
}
