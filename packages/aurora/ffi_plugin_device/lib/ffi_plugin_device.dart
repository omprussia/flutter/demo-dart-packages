// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'dart:ffi';

import 'package:device_platform_interface/device_platform_interface.dart';
import 'package:ffi/ffi.dart';

import 'ffi_plugin_device_bindings_generated.dart';

class FFIPluginDevice extends DevicePlatform {
  /// Метод который выполнится при старте приложения
  /// В этом методе можно установить платформо-зависимый плагин
  static void registerWith() {
    DevicePlatform.instance = FFIPluginDevice();
  }

  /// Привязки к нативным функциям в [_dylib].
  final PluginDeviceBindings _bindings = PluginDeviceBindings(
    DynamicLibrary.open('libffi_plugin_device.so'),
  );

  /// Реализация метода инетфрайса [DevicePlatform]
  @override
  Future<String?> get deviceName async {
    // Get deviceName
    final deviceName = _bindings.getDeviceName().cast<Utf8>().toDartString();
    // Return value
    return deviceName == '' ? null : deviceName;
  }
}
