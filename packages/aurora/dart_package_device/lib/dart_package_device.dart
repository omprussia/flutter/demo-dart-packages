// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:dart_package_device/ru_omp_device_info_features.dart';
import 'package:dbus/dbus.dart';
import 'package:device_platform_interface/device_platform_interface.dart';

class DartPackageDevice extends DevicePlatform {
  /// Метод который выполнится при старте приложения
  /// В этом методе можно установить платформо-зависимый плагин
  static void registerWith() {
    DevicePlatform.instance = DartPackageDevice();
  }

  /// Реализация метода инетфрайса [DevicePlatform]
  @override
  Future<String?> get deviceName async {
    // Инициализация клента D-Bus
    final client = DBusClient.session();

    // Инициализация обьекта
    final features = RuOmpDeviceinfoFeatures(
      client,
      'ru.omp.deviceinfo',
      DBusObjectPath('/ru/omp/deviceinfo/Features'),
    );

    // Выполнение метода
    final deviceName = await features.callGetDeviceModel();

    // Закрытие клиента D-Bus
    await client.close();

    // Возвращение результата
    return deviceName == '' ? null : deviceName;
  }
}
