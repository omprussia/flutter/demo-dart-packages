/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
package ru.auroraos.plugin_device_android

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

class PluginDeviceAndroidPlugin : FlutterPlugin, MethodCallHandler {
    /**
     * MethodChannel, который будет осуществлять связь между Flutter и родным Android
     * Эта локальная ссылка служит для регистрации плагина в Flutter Engine и
     * отмены его регистрации когда Flutter Engine отсоединен от Activity
     */
    private lateinit var channel: MethodChannel

    /**
     * Инициализация [MethodChannel].
     */
    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "plugin_device_android")
        channel.setMethodCallHandler(this)
    }

    /**
     * Метод onMethodCall будет выполняться при вызове MethodChannel из Dart плагина.
     * По названию передаваемому из плагина можно вызвать нужный платфором-зависимый метод.
     */
    override fun onMethodCall(call: MethodCall, result: Result) {
        if (call.method == "getDeviceName") {
            result.success("${android.os.Build.BRAND.capitalize()} ${android.os.Build.MODEL}")
        } else {
            result.notImplemented()
        }
    }

    /**
     * Если FlutterPlugin удаляется из FlutterEngine перехода PluginRegistry.remove
     * или уничтожается FlutterEngine, FlutterEngine будет вызываться onDetachedFromEngine.
     */
    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
