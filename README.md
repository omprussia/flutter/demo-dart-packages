# Flutter Device

![108x108.png](hello_world/aurora/icons/108x108.png)

The goal of the project is to demonstrate ways to create Dart packages for the Aurora OS platform.
For this purpose, a package was developed that makes a request to the system to obtain the device name from the running application.
The package for the Aurora OS platform is implemented in three possible types of Dart packages:

*	**Dart package** - Packages written in Dart, for example package [`path`](https://pub.dev/packages/path). They may contain Flutter-specific functions and have dependencies on the Flutter framework, limiting their use to Flutter only, for example package [`fluro`](https://pub.dev/packages/fluro).
*	**Plugin package** - A specialized Dart package containing an API written in Dart, combined with one or more platform-specific implementations. Plugin packages can be written for Android (using Kotlin or Java), Aurora OS (using C++), iOS (using Swift or Objective-C), Web, macOS, Windows or Linux, or any combination thereof.
*	**FFI Plugin package** - A specialized Dart package containing an API written in Dart, with one or more platform-specific implementations using [Dart FFI](https://dart.dev/guides/libraries/c-interop).

To demonstrate interaction with another platform, a platform-dependent plugin for the Android platform was added.
Plugins are implemented through a single platform interface.

## Structure

![Diagram1.jpg](data/Diagram1.jpg)

## Usage

To change the type of plugin for the Aurora OS, you need to specify the desired plugin in the `default_package` field in `pubspec.yaml` of the general package (flutter_device):

```yaml
flutter:  
  plugin:  
    platforms:  
      aurora:  
      default_package: <dart_package_device|plugin_device|ffi_plugin_device>
```

* dart_package_device - Dart package
* plugin_device - Plugin package
* ffi_plugin_device - FFI Plugin package
