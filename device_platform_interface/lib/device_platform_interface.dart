// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

/// Класс которые будут реализовать платформо-зваисимые пакеты
abstract class DevicePlatform extends PlatformInterface {
  /// Конструктор PlatformInterface
  DevicePlatform() : super(token: _token);

  /// Токен по умолчанию
  static final Object _token = Object();

  /// Реализация пакета
  static DevicePlatform? _instance;

  /// Публичная переменная для получения пакета
  static DevicePlatform get instance => _instance!;

  /// Метод иницаилизации платфоромо-зависимого пакета
  static set instance(DevicePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  /// Метод возвращающий названия устройства
  Future<String?> get deviceName {
    throw UnimplementedError('deviceName() has not been implemented.');
  }
}
